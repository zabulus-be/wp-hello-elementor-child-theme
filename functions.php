<?php

include( get_stylesheet_directory() . '/_inc_functions/acf-options.php' );
include( get_stylesheet_directory() . '/_inc_functions/remove-bloat.php' );
include( get_stylesheet_directory() . '/_inc_functions/remove-elementor-bloat.php' );
include( get_stylesheet_directory() . '/_inc_functions/optimize-speed.php' );

// load css stylesheet
// use style.css for development
// user style.min.css for production
function load_style() {
	wp_enqueue_style( 
		'style', 
		get_stylesheet_directory_uri() . '/style.css', 
		false, 
		date( 'dmYhi', filemtime( get_stylesheet_directory() . '/style.css' ) )
	);
}
add_action( 'wp_enqueue_scripts', 'load_style' );

// register menu(s)
// function register_my_menus() {
// 	register_nav_menus(
// 		array(
// 			'secondary-menu' => __( 'Secondary Menu' ),
// 			'mobile-menu' => __( 'Mobile Menu' ),
// 			'wpml-menu' => __( 'WPML Menu' )
// 		)
// 	);
// }
// add_action( 'init', 'register_my_menus' );

// GF scroll to form after submit
if ( class_exists( 'GFCommon' ) ) { // GF plugin is activated
	add_filter('gform_confirmation_anchor', '__return_true' );
}

////////////////////////////
// custom functions below //
////////////////////////////



?>
