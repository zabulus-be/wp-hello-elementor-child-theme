<?php

// ACF options page
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Site Opties',
		'menu_title'	=> 'Site Opties',
		'menu_slug' 	=> 'site-options',
		'capability'	=> 'edit_posts',
		'icon_url'		=> 'dashicons-star-filled',
		'redirect'		=> false
	));
}
