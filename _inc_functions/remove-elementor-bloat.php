<?php

// Remove unused scripts loaded by Elementor
function wpse_elementor_frontend_scripts() {
    // you can change yourself for which pages the conditional logic below accounts
	if(is_front_page()) {
		// dequeue WP block library
		wp_dequeue_style( 'wp-block-library-theme' );

		// dequeue WP block library
		wp_dequeue_style( 'wp-block-library' );

		// dequeue and deregister swiper
		wp_dequeue_script( 'swiper' );
		wp_deregister_script( 'swiper' );

		// dequeue and deregister elementor-dialog
		wp_dequeue_script( 'elementor-dialog' );
		wp_deregister_script( 'elementor-dialog' );

		// dequeue and deregister elementor-frontend
		wp_dequeue_script( 'elementor-frontend' );
		wp_deregister_script( 'elementor-frontend' );

		// re-register elementor-frontend without the elementor-dialog/swiper dependency.
		$suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';
		wp_register_script(
				'elementor-frontend',
				ELEMENTOR_ASSETS_URL . 'js/frontend' . $suffix . '.js',
				[
					'elementor-frontend-modules',
					'elementor-waypoints'
				],
				ELEMENTOR_VERSION,
				true
			);
	}
}
add_action( 'wp_enqueue_scripts', 'wpse_elementor_frontend_scripts' );
