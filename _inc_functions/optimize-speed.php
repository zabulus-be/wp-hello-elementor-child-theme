<?php

// Defer parsing of url
// https://www.hostinger.com/tutorials/wordpress/how-to-defer-parsing-of-javascript-in-wordpress
// function defer_parsing_of_js ( $url ) {
// 	if ( FALSE === strpos( $url, '.js' ) ) return $url;
// 	if ( strpos( $url, 'jquery.js' ) ) return $url;
// 	return "$url' defer='defer";
// }
// add_filter( 'clean_url', 'defer_parsing_of_js', 11, 1 );
